<?php
/*
 * This file receives the contact form message, and
 * json encodes the command before sending it back to sMod
 */

session_start();

require_once 'creds.php';
require_once 'sModSend.php';

$command = array();

if(isset($_POST['subscribe'])){
    $command['mode'] = 'subscribe';
    $command['user']['name'] = $_POST['your_name'];
    $command['user']['first_name'] = explode(' ',$_POST['your_name'],1);
    $command['user']['last_name'] = explode(' ',$_POST['your_name'],1);
    $command['user']['email'] = $_POST['your_email'];
    $command['group']['sequence'] = $_POST['sequence'];
}

if(isset($_POST['unsubscribe'])){
    //TODO implement logon
}

$sModSend = new sModSend($company_id,$company_key,$command);

$result = $sModSend->json;

header("Location: /p/thanks_for_subscribing ");

?>