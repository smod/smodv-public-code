<?php

/* this file contains the site's main template
* you should add your own template here, but make sure you don't
* remove the first 2 lines after this comment, also, be sure to update
* the company key with your actual company key, and the company id with
* your actual company_id (available from www.smodv.com
*/


if(!file_exists('creds.php')){
    require 'installer.php';
}
else {
require_once 'sMod.php';
require_once 'creds.php';
$sMod = new sMod($company_id,$company_key);
//print_r($sMod);

/* now add your custom HTML template after this php closing tag */
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="user-scalable=yes, initial-scale=1"/>
<?php echo $sMod->meta_data ?>
<!--All JS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
<script src="//d1z4p5nas97osi.cloudfront.net/cdn/public-js.js"></script>
<script src="/js/js.js"></script>
<!--All CSS -->
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
<link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" rel="stylesheet" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet" />
<!-- Optional theme DELETE THIS NEXT LINE TO USE YOUR OWN THEME-->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css" rel="stylesheet" />

<link href="/css/css.css" rel="stylesheet" />
<!-- sort out old browsers -->
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<?php echo $sMod->analytics ?>
</head>
<body>
<div id="wrap">
    <div class="navbar navbar-default navbar-static-top navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header navbar-inverse ">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="/"><img src="/img/logo.png" alt="sMod Responsive Web Development Tasmania" /></a>
                <div class="navbar-collapse collapse navbar-right pull-right ">
                    <?php echo $sMod->positions->top_menu->menus ?>
                    <?php //echo $sMod->positions->top_menu->modules ?>
                </div>
            </div>
        </div>
    </div>
    <?php if(isset($sMod->positions->Jumbo)) { ?>
    <section>
        <div class="jumbotron">
            <?php echo $sMod->positions->Jumbo->modules ?>
        </div>
    </section>
    <?php } ?>
    <section>
        <div class="container">
            <div class="row-fluid">
                <div class="col-lg-4">
                <?php if(isset($sMod->positions->module_4)){ ?>
                    <?php echo $sMod->positions->module_4->modules ?>
                <?php } ?>
                </div>
                <div class="col-lg-4">
                <?php if(isset($sMod->positions->module_5)){ ?>
                    <?php echo $sMod->positions->module_5->modules ?>
                <?php } ?>
                </div>
                <div class="col-lg-4">
                <?php if(isset($sMod->positions->module_6)){ ?>
                    <?php echo $sMod->positions->module_6->modules ?>
                <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row-fluid">
                <?php
                echo $sMod->page
                ?>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row-fluid">
                <div class="col-lg-4">
                <?php if(isset($sMod->positions->module_5)){ ?>
                    <?php echo $sMod->positions->module_1->modules ?>
                <?php } ?>
                </div>
                <div class="col-lg-4">
                <?php if(isset($sMod->positions->module_5)){ ?>
                    <?php echo $sMod->positions->module_2->modules ?>
                <?php } ?>
                </div>
                <div class="col-lg-4">
                <?php if(isset($sMod->positions->module_5)){ ?>
                    <?php echo $sMod->positions->module_3->modules ?>
                <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>
    <footer id="footer">
        <div class="container">
            <div class="row-fluid">
                <?php echo $sMod->copyright ?>
            </div>
        </div>
    </footer>
</body>
</html>


<?php
} //don't remove this or bad things will happen!!! ?>