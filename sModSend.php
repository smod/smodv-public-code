<?php
/*
 * This file contains the class that communicates with the sMod
 * api which receives commands.
 */

session_start();

class sModSend {

    private $smod_url = 'http://smodv-env.elasticbeanstalk.com/';
    private $company_id;
    private $company_key;
    private $session_id;

    public $json;
    private $command;

    public function __construct($company_id,$company_key,$command){
        $this->company_id = $company_id;
        $this->company_key = $company_key;
        $this->session_id = session_id();
        $this->getJSON($command);
    }

    private function getJSON($command){
        $this->buildCommandJSON($command);
        $final_command = json_encode($this->command);
        $url = $this->smod_url.'action.php';

        $ch = curl_init($url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        curl_setopt($ch,CURLOPT_POST,TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS,  "command=$final_command");
        $this->json = curl_exec($ch);
        curl_close($ch);
    }

    private function buildCommandJSON($command){
        $this->command['company_id'] = $this->company_id;
        $this->command['company_key'] = $this->company_key;
        if($command['mode'] == 'send_contact_email'){
            $this->command['email'] = $this->buildContactEmail($command);
        }
        if($command['mode'] == 'logon'){
            //TODO implement logon
        }
        if($command['mode'] == 'subscribe'){
            $this->command['subscription'] = $command;
        }
    }

    private function buildContactEmail($command){
        $return = array();
        $return['mode'] = 'send_contact_email';
        $return['cc'] = $command['your_email'];
        $return['subject'] = 'Enquiry received on '.$_SERVER['HTTP_HOST'];
        $return['replyto'] = $command['your_email'];
        $return['text'] = '<p>Hello, a new enquiry has been received, the details are as follows.</p>';
        $return['text'] .= '<p>Name: '.$command['your_name'].'<br />';
        $return['text'] .= 'Phone: '.$command['your_phone'].'<br />';
        $return['text'] .= 'Email: '.$command['your_email'].'</p>';
        $return['text'] .= '<p>Message: <br />'.$command['your_message'].'</p>';
        return $return;
    }
}

?>