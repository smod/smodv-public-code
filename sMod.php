<?php

/* this file contains the entire application logic
* use of this file is governed by the sMod terms of use
* which are available at www.smodv.com/terms-of-use
*/

session_start();

include_once 'updater.php';
include_once 'Helper.php';

class sMod {

    private $smod_url = 'http://www.smodv.com/api_websites/fetch.json'; //uncomment before merging
    private $template;
    private $session_id;
    private $json;
    private $company_id;
    private $company_key;
    public $decoded;

    public $company;
    public $logo;
    public $positions;
    public $menus;
    public $modules;
    public $page;
    public $analytics;
    public $meta_data;
    public $copyright;
    public $api_url;

    public $mode;
    public $slug;
    public $slug1;
    public $slug2;
    public $slug3;
    public $slug4;
    public $slug5;

    public function __construct($company_id,$company_key,$template="default"){
        $this->template = $template;
        $this->company_id = $company_id;
        $this->company_key = $company_key;
        $this->session_id = session_id();
        $this->setSlugs();
        $this->getJSON();
        $this->decoded = json_decode($this->json);
        //print_r($this->decoded);
        $this->setMetaData();
        $this->setAnalytics();
        $this->setCopyright();
        $this->setLogo();
        $this->setPositions();
        $this->setPage();
        Updater::update();
    }

    private function setSlugs(){
        if(isset($_GET['mode'])){$this->mode = $_GET['mode'];}
        else {$this->mode = 'p';}
        if(isset($_GET['slug'])){$this->slug = $_GET['slug'];}
        else {$this->slug = '';}
        if(isset($_GET['slug1'])){$this->slug1 = $_GET['slug1'];}
        else {$this->slug1 = '';}
        if(isset($_GET['slug2'])){$this->slug2 = $_GET['slug2'];}
        else {$this->slug2 = '';}
        if(isset($_GET['slug3'])){$this->slug3 = $_GET['slug3'];}
        else {$this->slug3 = '';}
        if(isset($_GET['slug4'])){$this->slug4 = $_GET['slug4'];}
        else {$this->slug4 = '';}
        if(isset($_GET['slug5'])){$this->slug5 = $_GET['slug5'];}
        else {$this->slug5 = '';}
    }

    private function getJSON(){
        $url = $this->smod_url.'?company_id='.$this->company_id.
            '&company_key='.$this->company_key.
            '&mode='.$this->mode.
            '&slug='.$this->slug.
            '&slug1='.$this->slug1.
            '&slug2='.$this->slug2.
            '&slug3='.$this->slug3.
            '&slug4='.$this->slug4.
			'&slug5='.$this->slug5.
            '&session_id='.$this->session_id;
        $this->api_url = $url;
        $ch = curl_init($url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);
        $this->json = curl_exec($ch);
        curl_close($ch);
    }

    private function setMetaData(){
        $meta_data_raw = $this->decoded->page->meta_data;
        $this->meta_data = '';
        foreach($meta_data_raw as $meta_data_item){
            if($meta_data_item->meta_data_type == 'title'){
                $this->meta_data .= '<title>'.strip_tags($meta_data_item->meta_data_text).'</title>'."\r\n";
            }
            else {
                if(substr($meta_data_item->meta_data_type,0,2) == 'og'){
                    $this->meta_data .= '<meta property="'.$meta_data_item->meta_data_type.'" content="'.strip_tags($meta_data_item->meta_data_text).'" />'."\r\n";
                }
                else {
                    $this->meta_data .= '<meta name="'.$meta_data_item->meta_data_type.'" content="'.strip_tags($meta_data_item->meta_data_text).'" />'."\r\n";
                }
            }
        }
    }

    private function setAnalytics(){
        $this->analytics = $this->decoded->company->analytics."\r\n";
    }

    private function setCopyright(){
        $this->copyright = 'Copyright &copy; '.date('Y').' '.$this->decoded->company->name.' All Rights Reserved - Website powered by <a href="http://www.smodv.com">sMod Content Management</a>';
    }

    private function setLogo(){
        if(isset($this->decoded->company_logo)){
            $this->logo = $this->decoded->company_logo;
        }
    }

    private function setPositions(){
        $this->positions = $this->decoded->positions;
    }

    private function setPage(){
        if(!in_array($this->mode,array("c","d","i","o","p","s",'cart','account'))){
            header("Location: /p/404",404);
        }
        $template = 'template_'.$this->mode;
        $include = 'templates/'.$this->template.'/'.$this->mode.'.php';

        include_once $include;

        $template_components =  array('address', 'audio', 'device_id', 'email_address','file','image','phone',
            'product_details','social','video','website','search');

        foreach ($template_components as $component) {
            $include = 'templates/'.$this->template.'/template_'.$component.'.php';
            include_once($include);
        }

        $Template = new $template($this->decoded->page,$this->mode,$this->slug,$this->slug1,$this->slug2,$this->slug3,$this->slug4,$this->slug5,$this->decoded->positions);
        $this->page = $Template->output;
    }

}
