<?php

class sModHelper {

    private $aws_base = '//d1z4p5nas97osi.cloudfront.net/shared_images/paperclip_images/';
    //private static $aws_base_static = '//s3.amazonaws.com/smod-external/shared_images/paperclip_images/';
    private static $aws_base_static = '//d1z4p5nas97osi.cloudfront.net/shared_images/paperclip_images/';

    public static function getPaperclipPaths($class_name,$object,$size='small'){
    //for images we add the 'small' sub directory in the path and change extension to .jpg
        if ($class_name=='image') {
            $aws_link = self::sortOutPaperclipIdString($object);
            $filename = str_replace(array(".png",".PNG",".gif",".GIF",".jpeg",".JPEG"),".jpg",$object->paperclip_image_file_name);
            $file_path = $aws_link.'/'.$size.'/'.$filename;
        } else if ($class_name=='file') {
            $file_path = self::sortOutPaperclipIdStringForFile($object);
        }
        return $file_path;
    }

    public static function sortOutPaperclipIdString($object){
        $id_string = '';
        if(strlen($object->id) < 4){
            $id_string = self::$aws_base_static.'000/000/'.$object->id;
        }
        else if(strlen($object->id) == 4){
            $id_string = self::$aws_base_static.'000/00'.substr($object->id,0,1).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 5){
            $id_string = self::$aws_base_static.'000/0'.substr($object->id,0,2).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 6){
            $id_string = self::$aws_base_static.'000/'.substr($object->id,0,3).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 7){
            $id_string = self::$aws_base_static.'00'.substr($object->id,0,1).'/'.substr($object->id,1,3).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 8){
            $id_string = self::$aws_base_static.'0'.substr($object->id,0,2).'/'.substr($object->id,2,3).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 9){
            $id_string = self::$aws_base_static.substr($object->id,0,3).'/'.substr($object->id,3,3).'/'.substr($object->id,-3);
        }
        return $id_string;
    }

    public static function sortOutPaperclipIdStringForFile($object){
        //$aws_base = 'http://s3.amazonaws.com/smod-external/shared_files/paperclip_files/';
        $aws_base = self::$aws_base_static;
        $id_string = '';
        if(strlen($object->id) < 2){
            $id_string = $aws_base.'000/000/00'.$object->id;
        }
        else if(strlen($object->id) < 3){
            $id_string = $aws_base.'000/000/0'.$object->id;
        }
        else if(strlen($object->id) < 4){
            $id_string = $aws_base.'000/000/'.$object->id;
        }
        else if(strlen($object->id) == 4){
            $id_string = $aws_base.'000/00'.substr($object->id,0,1).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 5){
            $id_string = $aws_base.'000/0'.substr($object->id,0,2).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 6){
            $id_string = $aws_base.'000/'.substr($object->id,0,3).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 7){
            $id_string = $aws_base.'00'.substr($object->id,0,1).'/'.substr($object->id,1,3).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 8){
            $id_string = $aws_base.'0'.substr($object->id,0,2).'/'.substr($object->id,2,3).'/'.substr($object->id,-3);
        }
        else if(strlen($object->id) == 9){
            $id_string = $aws_base.substr($object->id,0,3).'/'.substr($object->id,3,3).'/'.substr($object->id,-3);
        }

        return  $id_string. "/original/" . $object->paperclip_file_file_name;
    }
}