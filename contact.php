<?php
/*
 * This file receives the contact form message, and
 * json encodes the command before sending it back to sMod
 */

session_start();

require_once 'creds.php';
require_once 'sModSend.php';

$command = array();

if(isset($_POST['send_message'])){
    $command['mode'] = 'send_contact_email';
    $command['your_name'] = $_POST['your_name'];
    $command['your_email'] = $_POST['your_email'];
    $command['your_phone'] = $_POST['your_phone'];
    $command['your_message'] = $_POST['your_message'];
    $command['sequence'] = $_POST['sequence'];
}

if(isset($_POST['custom_form'])){
    $command['mode'] = 'send_contact_email';
    $command['your_name'] = $_POST['your_name'];
    $command['your_email'] = $_POST['your_email'];
    $command['your_phone'] = $_POST['your_phone'];
    $command['your_message'] = $_POST['your_message'];
    foreach($_POST as $key => $value){
        if(!in_array($key,array('your_name','your_email','your_phone','your_message'))){
            $command['your_message'] .= "\r\n"."<br />";
            $command['your_message'] .= $key.'  '.$value;
        }
    }
    $command['sequence'] = $_POST['sequence'];
}

if(isset($_POST['logon'])){
    //TODO implement logon
}

$sModSend = new sModSend($company_id,$company_key,$command);

$result = $sModSend->json;

header("Location: /p/thanks ");