<?php

/*
 * This file simply calls the update repository, gets the update, and unzips it.
 * This file only runs once per day.
 * */

class Updater {

    public static  function update(){
        self::readLastUpdatedFromFile();
    }

    private static function readLastUpdatedFromFile(){
        if(date('H') == 21){
            $string = file_get_contents("last_updated.txt");
            if($string < (time() - 86400)){
            self::fetchUpdate();
            }
        }
    }

    private static function fetchUpdate(){
        exec("wget -N https://bitbucket.org/aaronbelsham/smodv-public-code-updates/get/version2.zip && unzip version2.zip && rm -f version2.zip && cd aaronbelsham* && echo Yes | cp -R * ../ && cd ../ && rm -R -f aaronbelsham*");
        self::writeLastUpdatedToDisk();
    }

    private static function writeLastUpdatedToDisk(){
        $handle = fopen("last_updated.txt","w");
        $contents = time();
        fwrite($handle,$contents);
        fclose($handle);

    }
}
?>