<?php
/*
 * This file creates the page output.
 * In order to edit this, and NOT have it overwrite your existing code, please
 * make sure you copy all files into a folder with the name of the template you
 * want to call, e.g "custom" and then change the template variable in your index.php file.
 * All you need to do is edit the embedded html in the buildLayout() function.
 * */

class template_o {

    private $raw_page;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
	private $slug5;
    private $plugins;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$slug5,$plugins){
        $this->plugins = $plugins;
        $this->page = $page;
        $this->raw_page = $page->raw_page;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
		$this->slug5 = $slug5;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '<div class="page_output">';
		$output .= $this->getParent($this->mode, $this->slug, $this->slug1, $this->slug2, $this->slug3, $this->slug4, $this->slug5);
        $output .= '<h1>'.$this->raw_page->organisation->organisation_name.'</h1>';
        $output .= template_image::buildOutput($this->raw_page->shared_images);
        $output .= '<div id="content_organisation"><p>'.str_replace(array("\r","\n"),"<br />",$this->raw_page->organisation->content).'</p></div>
        <div id="linked_organisation_items">
            '.template_phone::buildOutput($this->raw_page->shared_phones).'
            '.template_social::buildOutput($this->raw_page->shared_socials).'
            '.template_email_address::buildOutput($this->raw_page->shared_email_addresses).'
            '.template_website::buildOutput($this->raw_page->shared_websites).'
            '.template_video::buildOutput($this->raw_page->shared_videos).'
            '.template_address::buildOutput($this->raw_page->shared_addresses).'</div>
            <div>
            <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5313181a22734502"></script>
<!-- AddThis Button END -->
            </div></div>';
        return $output;
    }

	private	function getParent($mode, $slug, $slug1, $slug2, $slug3, $slug4, $slug5){
		if($mode == 'o'){
			$mode = 'd';
		}
		$output = '';
		if(isset($slug1) && !empty($slug1)){
			if(isset($slug2) && !empty($slug2)){
				if(isset($slug3) && !empty($slug3)){
					if(isset($slug4) && !empty($slug4)){
						if(isset($slug5) && !empty($slug5)){
							$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '/' . $slug3 . '/' . $slug4 . '" class="btn btn-default">Go Back!</a>';
						} else {
							$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '/' . $slug3 . '" class="btn btn-default">Go Back!</a>';
						}
					} else {
						$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '" class="btn btn-default">Go Back!</a>';
					}
				} else {
					$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '" class="btn btn-default">Go Back!</a>';
				}		
			} else {
				$output = '<a href="/'.$mode . '/' . $slug . '" class="btn btn-default">Go Back!</a>';
			}
		}
		return $output;	
	}

}
