<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:54 PM
 */

class template_image {
    public static function buildOutput($images) {
        $output = '
<div id="carousel-organisation" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">';
  $i = 0;
  foreach ($images as $image){
      $image_path = sModHelper::getPaperclipPaths('image',$image,'large');
      if(substr($image_path,-6) != '_l.jpg'){
            $output .= '
    <li data-target="#carousel-example-generic" data-slide-to="'.$i.'"';
      if($i==0){
          $output .= ' class="active" ';
      }
      $output .= '></li>
    ';
    $i++;
      }
  }
  $output .= '</ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner">';
  $i = 0;
  foreach ($images as $image){
    $image_path = sModHelper::getPaperclipPaths('image',$image,'large');
    if(substr($image_path,-6) != '_l.jpg'){
    if($i == 0){
        $output .= '
    <div class="item active">
      <img src="'.$image_path.'"/>
    </div>';
    }
    else {
        $output .= '
    <div class="item">
      <img src="'.$image_path.'"/>
    </div>';
    }
    $i++;
    }
   }
  $output .= '</div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-organisation" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-organisation" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>';
        return $output;
    }
} 