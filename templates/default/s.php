<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 3/02/2014
 * Time: 1:23 PM
 */

class template_s {
    private $raw_page;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
	private $slug5;
    private $plugins;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$slug5,$plugins){
        $this->plugins = $plugins;
        $this->raw_page = $page->raw_page;
        $this->page_results = $this->raw_page->CmsPage;
        $this->organisation_results = $this->raw_page->CrmOrganisation;
        $this->group_results = $this->raw_page->CrmGroup;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
		$this->slug5 = $slug5;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '
<div class="page_output">';
        $output .= '<p><h3>Search Results</h3></p>';
        //$output .= $this->raw_page->content.'
        $output .= '
        '.$this->buildSearchResults().'
</div>';
        return $output;
    }

    private function buildSearchResults() {

        $output = '
    <div id="search_results_id" class="search_results_container">';

        $output .= '<p><h4>Pages</h4></p>';
        $output .= '
        <div id="page_search_results_id" class="page_search_results_container">
            <ul>';
        foreach ($this->page_results as $page_result){
            $output .= '
                <li>
                    <p><a href="/p/'.$page_result->alias.'">'.$page_result->name.'</a><br />'.strip_tags(substr($page_result->content,200)).'</p>
                </li>';
        }
        $output .= '
            </ul>
        </div>';


        $output .= '<p><h4>Organisations</h4></p>';
        $output .= '
        <div id="organisation_search_results_id" class="organisation_search_results_container">
            <ul>';
        foreach ($this->organisation_results as $organisation_result){
            $output .= '
                <li>
                    <p><a href="/o/'.$organisation_result->alias.'">'.$organisation_result->organisation_name.'</a><br />'.strip_tags(substr($organisation_result->content,200)).'</p>
                </li>';
        }
        $output .= '
            </ul>
        </div>';


        $output .= '<p><h4>Groups</h4></p>';
        $output .= '
        <div id="group_search_results_id" class="group_search_results_container">
            <ul>';
        foreach ($this->group_results as $group_result){
            $output .= '
                <li>
                    <p><a href="/d/'.$group_result->alias.'">'.$group_result->name.'</a><br />'.strip_tags(substr($group_result->content,200)).'</p>
                </li>';
        }
        $output .= '
            </ul>
        </div>';

        $output .= '
    </div>';

        return $output;
    }

    private function calculateSubItemURL($alias,$mode='p'){
        return  '/'.$mode.'/'.$alias;
    }

} 
