<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:52 PM
 */

class template_device_id {
    public static function buildOutput($device_ids) {
        $output = '
        <div id="device_ids_list_id" class="device_ids_list_container">';
        $output .= '
            <ul>';
        foreach ($device_ids as $device_id){
            $output .= '
            <li>
                <p>Device ID '.$device_id->device_id.'</p>
            </li>';
        }
        $output .= '
            </ul>';
        $output .= '
        </div>';
        return $output;
    }
} 