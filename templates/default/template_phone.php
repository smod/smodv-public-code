<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:54 PM
 */

class template_phone {
    public static function buildOutput($phones) {
        $output = '
        <div id="phones_list_id" class="phones_list_container">';
        $output .= '
            <ul>';
        $processed = array();
        foreach ($phones as $phone){
            if($processed[$phone->phone_number]!== true){
                $output .= '
                <li>
                    <p>'.$phone->description.'<br />'.
                    'PH : '.$phone->phone_number.'<br />'.
                    '</p>
                </li>';
            }
            $processed[$phone->phone_number] = true;
        }
        $output .= '
            </ul>';
        $output .= '
        </div>';
        return $output;
    }


}