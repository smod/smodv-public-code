<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:55 PM
 */

class template_social {
    public static function buildOutput($socials) {
        $output = '
        <div id="socials_list_id" class="socials_list_container">';
        $output .= '
            <h5>Social Links</h5>
            <ul>';
        $processed = array();
        foreach ($socials as $social){
            if($processed[$social->social_network]!== true){
                if(!in_array($social->social_network,array('ATDW1_NATIVE_PRODUCT_CATEGORY_ID','ATDW1_NATIVE_PRODUCT_ID','ATDW1_PRODUCT_ID','LICENCE','THELIST','TXA_DEFAULT','TQUAL'))){
                    $output .= '
            <li>
                <a href="'.$social->social_link.'" target="_blank">'.ucfirst($social->social_network).'</a>
            </li>';
                $processed[$social->social_network] = true;
                }
            }
        }
        $output .= '
            </ul>
        </div>';
        return $output;
    }
} 