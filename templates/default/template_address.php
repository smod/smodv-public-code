<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:51 PM
 */

class template_address {
    public static function buildOutput($addresses) {
        $output = '
        <div id="addresses_list_id" class="addresses_list_container">';
        $markers = '';
        $output .= '
            <ul>';
        $processed = array();
        foreach ($addresses as $address){
            $address_string = $address->line1.' '.$address->line2.' '.$address->suburb.' '.$address->state.' '.$address->suburb.' '.$address->country;
            if($processed[$address_string] !== true){
            if($address->address_type != 'POSTAL') {
            $output .= '
            <li>
               <h5>Address</h5>'.
                $address->line1.'<br />';
                $address->line2 == '' ?: $output.= '' | $output .= $address->line2.'<br />';
                $output .= $address->suburb.' '.$address->state.'<br />'.
                $address->suburb.'<br />'.
                $address->country.'<br />'.
                '
            </li>';
            $markers .= '{
                        address: \''.$address->line1.' '.$address->line2.' '.$address->suburb.' '.$address->state.' '.$address->suburb.' '.$address->country.'\',
                        html: \'<p>'.$address->line1.' '.$address->line2.' '.$address->suburb.' '.$address->state.' '.$address->suburb.' '.$address->country.'</p>\',
                        popup: false,
                        },';
            }
                $processed[$address_string] = true;
            }
        }
        $output .= '
            </ul>';
        $output.='
        </div>';
        if(count($addresses) > 0){
        $output.= '<div id="map_container">
        <script type=\'text/javascript\' src=\'http://maps.google.com/maps/api/js?sensor=true\'></script>
            <script>jQuery(document).ready(function($) {
                $(\'#gMapContact\').gMap({
                    controls: {
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true
                },
                maptype: \'ROADMAP\',
                scrollwheel: false,
                zoom: 10,
                markers: [
                    '.$markers.'
                ]

            });
            });
            </script><div id="gMapContact"></div></div>';
        }
        return $output;
    }
}