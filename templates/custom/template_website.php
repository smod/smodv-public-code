<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 23/01/2014
 * Time: 12:55 PM
 */

class template_website {
    public static function buildOutput($websites) {
        $output = '
        <div id="websites_list_id" class="websites_list_container">';
        $output .= '
            <h5>Website Links</h5>
            <ul>';
        $processed = array();
        foreach ($websites as $website){
            if($processed[$website->web_address]!== true){

                $output .= '
            <li>'.
                'Website : <a href="'.template_website::add_http_if_not_present($website->web_address).'" target="_blank">'.$website->web_address.'</a><br />'.
            '</li>';
                $processed[$website->web_address] = true;
            }
        }
        $output .= '
            </ul>';
        $output .= '
        </div>';

        if (count($processed)==0) $output = '';

        return $output;
    }
    private static function add_http_if_not_present($web_address) {
        if (strpos($web_address,'http://') === false){
            $web_address = 'http://'.$web_address;
        }
        return $web_address;
    }
}


