<?php
/**
 * Created by PhpStorm.
 * User: jethro
 * Date: 3/02/2014
 * Time: 1:23 PM
 */

class template_s {
    private $raw_page;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
	private $slug5;
    private $plugins;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$slug5,$plugins){
        $this->plugins = $plugins;
        $this->raw_page = $page->raw_page;
        $this->page_results = $this->raw_page->CmsPage;
        $this->organisation_results = $this->raw_page->CrmOrganisation;
        $this->group_results = $this->raw_page->CrmGroup;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
		$this->slug5 = $slug5;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '
<div class="page_output">';
        $output .= '<p><h3>Search Results</h3></p>';
        //$output .= $this->raw_page->content.'
        $output .= '
        '.$this->buildSearchResults().'
</div>';
        return $output;
    }

    private function buildSearchResults() {

        $output = '
    <div id="search_results_id" class="search_results_container">';
        $output .= '<p><h4>Pages</h4></p>';
        $output .= '
        <div id="page_search_results_id" class="page_search_results_container">
            <ul>';
        foreach ($this->page_results as $page_result){
            //var_dump($page_result);

            $output .= '
            <li class="sub_item_li">
                <h3>'.$page_result->name.'</h3>
                <p>';
            $output .= strip_tags(substr($page_result->content,0,300)).'
                </p>
                <p><a class="btn btn-large btn-primary" href="/p/'.$page_result->alias.'">Read More</a></p>
            </li>
            ';
        }
        $output .= '
            </ul>
        </div>';


        $output .= '<p><h4>Organisations</h4></p>';
        $output .= '
        <div id="organisation_search_results_id" class="organisation_search_results_container">
            <ul>';
        foreach ($this->organisation_results as $organisation_result){

            $listing_expired = false;
            $custom_fields = json_decode($organisation_result->custom_fields);
            if ($custom_fields->event_end_date != '') {
                if (date_parse_from_format('Y-m-d',$custom_fields->event_end_date) >= date('Y-m-d')) {
                    $listing_expired = true;
                }
            }
//            if($custom_fields->event_end_date != '' && date_parse_from_format('Y-m-d',$custom_fields->event_end_date) >= date('Y-m-d')){
            if (!$listing_expired) {

            $image_file_url = sModHelper::getPaperclipPaths('image',$organisation_result->shared_images[0],'medium');
            $output .= '
            <li class="sub_item_li">
                <h3>'.$organisation_result->organisation_name.' '.$this->getSuburbForSubOrganisation($organisation_result).'</h3>
                <p>';
                        //$output .= '<img src="'.$image_file_url.'" alt="" class="img-rounded" style="width:140px; margin: 10px; float: left;" />';
                        $output .= strip_tags(substr($organisation_result->content,0,300)).'
                </p>
                <p><a class="btn btn-large btn-primary" href="/o/'.$organisation_result->alias.'">Read More</a></p>
            </li>
            ';
            }

        }
        $output .= '
            </ul>
        </div>';


        $output .= '<p><h4>Groups</h4></p>';
        $output .= '
        <div id="group_search_results_id" class="group_search_results_container">
            <ul>';
        foreach ($this->group_results as $group_result){
            $image_file_url = sModHelper::getPaperclipPaths('image',$group_result->shared_images[0],'medium');
            $output .= '
            <li class="sub_item_li">
                <h3>'.$group_result->name.'</h3>
                <p>';
            //$output .= '<img src="'.$image_file_url.'" alt="" class="img-rounded" style="width:140px; margin: 10px; float: left;" />';
            $output .= strip_tags(substr($group_result->content,0,300)).'
                </p>
                <p><a class="btn btn-large btn-primary" href="/d/'.$group_result->alias.'">Read More</a></p>
            </li>
            ';

        }
        $output .= '
            </ul>
        </div>';

        $output .= '
    </div>';

        return $output;
    }

    private function calculateSubItemURL($alias,$mode='p'){
        return  '/'.$mode.'/'.$alias;
    }

    private function getSuburbForSubOrganisation($organisation){
        $suburb = $organisation->shared_addresses[0];
        return $suburb->suburb;
    }

} 
