<?php

/*
 * This file creates the page output.
 * In order to edit this, and NOT have it overwrite your existing code, please
 * make sure you copy all files into a folder with the name of the template you
 * want to call, e.g "custom" and then change the template variable in your index.php file.
 * All you need to do is edit the embedded html in the buildLayout() function.
 * */

class template_d  {
    private $raw_page;
    private $sub_groups;
    private $sub_organisations;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
	private $slug5;
    private $plugins;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$slug5,$plugins){
        $this->plugins = $plugins;
        $this->page = $page;
        $this->raw_page = $page->raw_page->group;
        $this->sub_groups = $page->sub_groups;
        $this->sub_organisations = $page->sub_organisations;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
		$this->slug5 = $slug5;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '<div class="page_output">';
		$output .= $this->getParent($this->mode, $this->slug, $this->slug1, $this->slug2, $this->slug3, $this->slug4, $this->slug5);
        $output .= '<h3>'.$this->raw_page->name.'</h3>';
        $output .= count($this->page->images)>0 ? '<img src="'.$this->page->images[0]->paperclip_image_file_name.'"/>' : '';
        $output .= $this->raw_page->content.'
    <div class="sub_items">
        <ul class="sub_items_ul">
            '.$this->buildSubItemsList().'
        </ul>
    </div>
    <div class="sub_items_organisation">
        <ul class="sub_items_ul">
            '.$this->buildSubOrganisationsList().'
        </ul>
    </div>
    <div>
            <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5313181a22734502"></script>
<!-- AddThis Button END -->
            </div>
</div>';
        return $output;
    }

    private function buildSubItemsList(){
        $output = '';
        foreach($this->sub_groups as $raw_page){
            $output .= '
            <a href="'.$this->calculateSubItemURL($raw_page->group->alias,$this->raw_page->alias,'d').'">
                <li class="sub_groups_ul_li">
                    '.$raw_page->group->name.'
                </li>
            </a>
';
        }
        return $output;
    }

    private function buildSubOrganisationsList(){
        $output = '';
        foreach($this->sub_organisations as $raw_page){
            $listing_expired = false;
            $custom_fields = json_decode($raw_page->custom_fields);
            if ($custom_fields->event_end_date != '') {
                if (date_parse_from_format('Y-m-d',$custom_fields->event_end_date) >= date('Y-m-d')) {
                    $listing_expired = true;
                }
            }
            if (!$listing_expired) {
                $image_file_url = sModHelper::getPaperclipPaths('image',$raw_page->shared_images[0],'medium');
                $output .= '
<li class="sub_item_li">
    <h3>'.$raw_page->organisation->organisation_name.' - '.$this->getSuburbForSubOrganisation($raw_page->organisation).'</h3>
    <p>';
            $output .= '<img src="'.$image_file_url.'" alt="" class="img-rounded" style="width:140px; margin: 10px; float: left;" />';
            $output .= strip_tags(substr($raw_page->organisation->content,0,300)).'
    </p>
    <p><a class="btn btn-large btn-primary" href="'.$this->calculateSubItemURL($raw_page->organisation->alias,$this->raw_page->alias,'o').'">Read More</a></p>
</li>
';
            }
        }
        return $output;
    }

	private function calculateSubItemURL($alias,$parent,$mode='p'){
        if($this->slug1 == ''){
            $url = '/'.$mode.'/'.$parent.'/'.$alias;
        }
        else if($this->slug2 == '' && $this->slug1 != ''){
            $url = '/'.$mode.'/'.$this->slug.'/'.$parent.'/'.$alias;
        }
        else if($this->slug3 == '' && $this->slug2 != ''){
            $url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$parent.'/'.$alias;
        }
        else if($this->slug4 == '' && $this->slug3 != ''){
			$url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$this->slug2.'/'.$parent.'/'.$alias;
        }
        else {
            $url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$this->slug2.'/'.$this->slug3.'/'.$parent.'/'.$alias;
        }
        return $url;
    }

    private function getSuburbForSubOrganisation($organisation){
        $suburb = $organisation->shared_addresses[0];
        return $suburb->suburb;
    }

	private	function getParent($mode, $slug, $slug1, $slug2, $slug3, $slug4, $slug5){
    	if($mode == 'o'){
    		$mode = 'd';
    	}
    	$output = '';
    	if(isset($slug1) && !empty($slug1)){
    		if(isset($slug2) && !empty($slug2)){
    			if(isset($slug3) && !empty($slug3)){
    				if(isset($slug4) && !empty($slug4)){
    					if(isset($slug5) && !empty($slug5)){
    						$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '/' . $slug3 . '/' . $slug4 . '" class="btn btn-default">Go Back!</a>';
    					} else {
    						$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '/' . $slug3 . '" class="btn btn-default">Go Back!</a>';
    					}
    				} else {
    					$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '" class="btn btn-default">Go Back!</a>';
    				}
    			} else {
    				$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '" class="btn btn-default">Go Back!</a>';
    			}
    		} else {
    			$output = '<a href="/'.$mode . '/' . $slug . '" class="btn btn-default">Go Back!</a>';
    		}
    	}
    	return $output;
    }

}

?>
