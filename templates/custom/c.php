<?php

class template_c {
    private $raw_page;
    private $sub_categories;
    private $sub_products;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
	private $slug5;
    private $plugins;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$slug5,$plugins){
        $this->plugins = $plugins;
        $this->page = $page;
        $this->raw_page = $page->raw_page->category;
        $this->sub_categories = $this->raw_page->sub_categories;
        $this->sub_products = $this->raw_page->sub_products;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
		$this->slug5 = $slug5;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '
<div class="page_output">';
        $output .= '<p><h3>'.$this->raw_page->name.'</h3></p>';
        $output .= count($this->page->images)>0 ? '<img src="'.$this->page->images[0]->paperclip_image_file_name.'"/>' : '';
        $output .= $this->raw_page->content.'
    <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style addthis_32x32_style">
            <a class="addthis_button_preferred_1"></a>
            <a class="addthis_button_preferred_2"></a>
            <a class="addthis_button_preferred_3"></a>
            <a class="addthis_button_preferred_4"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-515a9f1902a1a4b4"></script>
    <!-- AddThis Button END -->
            '.template_image::buildOutput($this->page->images).'
            '.template_audio::buildOutput($this->page->audios).'
            '.template_file::buildOutput($this->page->files).'
            '.template_video::buildOutput($this->page->videos).'
    <div class="sub_items">
        <ul class="sub_items_ul">
            '.$this->buildSubCategoriesList().'
        </ul>
    </div>
        <div class="sub_items">
        <ul class="sub_items_ul">
            '.$this->buildSubProductsList().'
        </ul>
    </div>
    <h6><em>Last updated '.$this->raw_page->updated_at.'</em></h6>
</div>';
        return $output;
    }

    private function buildSubCategoriesList(){
        $output = '';
        foreach($this->sub_categories as $raw_page){
            $output .= '
<li class="sub_item_li">
    <h3>'.$raw_page->name.'</h3>
    <p>';
            //TODO add image output
            $output .= substr($raw_page->content,700).'</p>
    <p><a class="btn btn-large" href="'.$this->calculateSubItemURL($raw_page->alias,$this->raw_page->alias,'d').'">Read More</a></p>
</li>
';
        }
        return $output;
    }
    private function buildSubProductsList(){
        $output = '';
        foreach($this->sub_products as $raw_page){
            $output .= '
<li class="sub_item_li">
    <h3>'.$raw_page->name.'</h3>
    <p>';
            //TODO add image output
            $output .= substr($raw_page->content,700).'</p>
    <p><a class="btn btn-large" href="'.$this->calculateSubItemURL($raw_page->alias,$this->raw_page->alias,'d').'">Read More</a></p>
</li>
';
        }
        return $output;
    }
 
	private function calculateSubItemURL($alias,$parent,$mode='p'){
        if($this->slug1 == ''){
            $url = '/'.$mode.'/'.$parent.'/'.$alias;
        }
        else if($this->slug2 == '' && $this->slug1 != ''){
            $url = '/'.$mode.'/'.$this->slug.'/'.$parent.'/'.$alias;
        }
        else if($this->slug3 == '' && $this->slug2 != ''){
            $url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$parent.'/'.$alias;
        }
        else if($this->slug4 == '' && $this->slug3 != ''){
			$url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$this->slug2.'/'.$parent.'/'.$alias;
        }
        else {
            $url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$this->slug2.'/'.$this->slug3.'/'.$parent.'/'.$alias;
        }
        return $url;
    }

}
?>
