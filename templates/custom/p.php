<?php
/*
* This file creates the page output.
* In order to edit this, and NOT have it overwrite your existing code, please
* make sure you copy all files into a folder with the name of the template you
* want to call, e.g "custom" and then change the template variable in your index.php file.
* All you need to do is edit the embedded html in the buildLayout() function.
* */

class template_p {
    private $raw_page;
    private $sub_items;
    public $output;
    private $mode;
    private $slug;
    private $slug1;
    private $slug2;
    private $slug3;
    private $slug4;
    private $slug5;

    public function __construct($page,$mode,$slug,$slug1,$slug2,$slug3,$slug4,$slug5){
        $this->page = $page;
        $this->raw_page = $page->raw_page->page;
        $this->sub_items = $page->sub_pages;
        $this->mode = $mode;
        $this->slug = $slug;
        $this->slug1 = $slug1;
        $this->slug2 = $slug2;
        $this->slug3 = $slug3;
        $this->slug4 = $slug4;
		$this->slug5 = $slug5;
        $this->output = $this->buildHTML();
    }

    private function buildHTML(){
        $output = '<div class="page_output">';
		$output .= $this->getParent($this->mode, $this->slug, $this->slug1, $this->slug2, $this->slug3, $this->slug4, $this->slug5);
        $output .= str_replace('{READMORE}','',$this->raw_page->content).'
        <div>
            <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_32x32_style">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
<a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5313181a22734502"></script>
<!-- AddThis Button END -->
            </div>
    <div class="sub_items">
        <ul class="sub_items_ul">
            '.$this->buildSubItemsList().'
        </ul>
    </div>
</div>';
        return $output;
    }

    private function buildSubItemsList(){
        $output = '';
        foreach($this->sub_items as $raw_page){
            $output .= '
<li class="sub_item_li">
    <h3>'.$raw_page->name.'</h3>';
if(strpos($raw_page->page->content,'{READMORE}')){
    $output .= '<p>'.substr($raw_page->page->content,0,strpos($raw_page->page->content,'{READMORE}')).'</p>';
}
else{
    $output .= '<p>'.substr($raw_page->page->content,0,500).'</p>';
}
    $output .= '
    <p><a class="btn btn-large btn-primary" href="'.$this->calculateSubItemURL($raw_page->page->alias,$this->raw_page->alias).'">Read More</a></p>
</li>
';
        }
        return $output;
    }

	private function calculateSubItemURL($alias,$parent,$mode='p'){
        if($this->slug1 == ''){
            $url = '/'.$mode.'/'.$parent.'/'.$alias;
        }
        else if($this->slug2 == '' && $this->slug1 != ''){
            $url = '/'.$mode.'/'.$this->slug.'/'.$parent.'/'.$alias;
        }
        else if($this->slug3 == '' && $this->slug2 != ''){
            $url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$parent.'/'.$alias;
        }
        else if($this->slug4 == '' && $this->slug3 != ''){
			$url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$this->slug2.'/'.$parent.'/'.$alias;
        }
        else {
            $url = '/'.$mode.'/'.$this->slug.'/'.$this->slug1.'/'.$this->slug2.'/'.$this->slug3.'/'.$parent.'/'.$alias;
        }
        return $url;
    }


	private	function getParent($mode, $slug, $slug1, $slug2, $slug3, $slug4, $slug5){
    	if($mode == 'o'){
    		$mode = 'd';
    	}
    	$output = '';
    	if(isset($slug1) && !empty($slug1)){
    		if(isset($slug2) && !empty($slug2)){
    			if(isset($slug3) && !empty($slug3)){
    				if(isset($slug4) && !empty($slug4)){
    					if(isset($slug5) && !empty($slug5)){
    						$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '/' . $slug3 . '/' . $slug4 . '" class="btn btn-default">Go Back!</a>';
    					} else {
    						$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '/' . $slug3 . '" class="btn btn-default">Go Back!</a>';
    					}
    				} else {
    					$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '/' . $slug2 . '" class="btn btn-default">Go Back!</a>';
    				}
    			} else {
    				$output = '<a href="/'.$mode . '/' . $slug . '/' . $slug1 . '" class="btn btn-default">Go Back!</a>';
    			}
    		} else {
    			$output = '<a href="/'.$mode . '/' . $slug . '" class="btn btn-default">Go Back!</a>';
    		}
    	}
    	return $output;
    }

}

?>
